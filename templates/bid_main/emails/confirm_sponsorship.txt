{% autoescape off %}
Confirm your sponsorship for a KDE Developer Account

Hi {{ user.full_name|default:user.email_to_confirm }}!

You have received this message because someoned added you as
sponsor for his KDE Developer request.

You can click on the link below to confirm or deny your
sponsorship.

{{ url }}

Note that this link will be valid for only 12 hours.
Kind regards,

The KDE Sysadmin team
{% endautoescape %}
