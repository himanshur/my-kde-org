"""
Local development settings.

Copy to settings.py and edit to suit your needs.
"""

# noinspection PyUnresolvedReferences
from mykdeorg.common_settings import *
import sys
import os
import dj_database_url
import environ

env = environ.Env(
    DEBUG=(bool, False),
    BLENDER_ID_ADDON_CLIENT_ID=(str, 'SPECIAL-SNOWFLAKE-57'),
    DEFAULT_FROM_EMAIL=(str, 'webmaster@localhost'),
    SECRET_KEY=(str, 'my_secret_key'),
    DATABASE_URL=(str, 'sqlite:///' + os.path.join(BASE_DIR, 'db.sqlite3')),
    ALLOWED_HOST=(str, '127.0.0.1'),
    DEFAULT_HOSTNAME=(str, 'https://gallien.kde.org'),
)

path = os.path.join(os.path.dirname(__file__), "..", '.env')

# reading .env file
env.read_env(path)

DEBUG = env('DEBUG')
DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL')

OIDC_ISS_ENDPOINT = "https://gallien.kde.org"

OIDC_RSA_PRIVATE_KEY = """-----BEGIN RSA PRIVATE KEY-----
MIIJJwIBAAKCAgEA6HR57Fz5PZSu3kEbu8ptRYmrrJy4fvmZgWTkN7/bcgU3S8vu
a6HstURb4OGZQmgaH8sg10o3oK/9GwikgrEECHM+xWJsgjWMTnePtgGV2B2J6Knl
0qrEtv+Eu+iS+Q2Dg27BzAevwFYzhNN8erL6wUqbGKg9C1aVNHGsE3i1onwFADVL
ymsE1E+r8dmFB2ald1lWmld6nl1LM2dYLkhWqZWlbVIvg92lDtAKhzi5aosGi/bH
AlSJuP/rkFWOvE0oyYD2qjewe9axtK3FMNw+vL0BwKyH83vg9RMzRGLOhmkmNrHZ
WoXdQfzck3y9366m0DSGND3NTKuAAVaW0mgTqPoEC90Y7dkb2u4D1eyjWCss09L2
sPde0HPB/6/04T+Vc1jCUpccRzcEAXWKe2i11CxCjPZDxAUqrwmrGHH8uzaSTBcS
TmL7ruY9ea/gR0m9zqvocRajNgmk0l7vIDoEjchegt/0d45rRaKQjgBW4uyMXbn/
e5uutaM+JY3pIUj8zq7eeBN/3ojKzHoloMaS/FvVMy2/sA/6s5Zq7SNIxk8/fKHA
9Dwuop/Jbz+w4Sfqgy6kIcuBcX8EBUrM+ZKCDFTLFJd868FslQjozy9GdSyeEOiN
Cz42IY5Gpl7ETyRWRZGM9xObYqfYp2jhPf+XU968ZOeH8BEUtERlPjSFmfcCAwEA
AQKCAgA82slUdJ6XXYZODxN4nSzwKHN2E+1E6IkkU8pfQe3sMqtgP3oiWVjDt6qp
+8WImgJE0oO9eOOJQKltW1zeDgN1Rt5nBDZN/EDIBgrsZgvZcRo6e8f719L56XjR
TLi1VLFaRjOb+2MqYibInikXYiW9Vo+1681XW4piGWQoENDUQc15WXqG+m9gOZ9V
CFIINvoYfZ3rd/1U2i2mvmZab3uol9GgBgUEa5EbiKFI9zOrTcwf9jHxTTpk4TLF
LXDZoQWNgyKQO1YLhZAuMdPOz8no1T5RN5m7x3GEF0uzmn5gt8RWS0cQqLzc2prQ
TbbTiUCIf0enA4rsiHVRZF7FpJOIiHGl2BEhIjaX/B3OuZ3efV9SR9KCnxYZxk8Y
O4Qt5R9ilQ8njlIIsTbJ6mqzuR4pqDiRkIA8DwRyFjsSaSPmcYEXjWBDvfro0Zu8
hkCI6uiViNggfxHkaW1Pixd8Hb9Fdpsm3DpPnoT+M9itpAJJ01W6h6Gy6MW3xYiY
PaBmrQRbVmApGGNRGDznjGweXyxQQvuAU11Q42TFJftqNj9Yc2MSFxYwhW35l00H
cQBcjfK9VS/0wmaYMLTDxnGfsIIXNvJzA4spl4gzPERoMKHqmraJt4np2EIWnQjn
nfof2of2fJ8Vn8MeN1q5c9iDcecvWKJ6ydBjveGOgen016lfgQKCAQEA+VPG00tn
WSaM1Oo+zK4X0ZeJZVMu58v7o/7dhXqgebGAFUlXE1q7WW3yU0qzRhNVbTc+jXAl
wz9/JlrDnPU9qOZHWmk8Fxlv+CWGb+B765binIXRmm04Ubi1Zm3b6PszTCC2dZI9
IcZR85Wmo7n+XRZsX659EXD4RME9kKHDiYqpSKEKSYJDoUGxxNoZRI4vDFtVZoIP
LdI6YoEJc6Q3u0n62OmU8eAOwpzV2x3ef4q5fkgNDwqYsW09wcqv9oZGz7OCoB2D
ndQ8h3B2hkEs4rn2Dlbvo7B9XDBHPkZHlPoVOug4bWch2zOggYpxfsKxrykKhV1W
Yq9U2ACa85yeNwKCAQEA7q0aJQTow7vL5soUHKbPWXOiU2zzPilAkKnGSb7xemps
RDUoQXfSRjC2rcvX5QM7QLRZih5RxMDt3gOzzSGjZun/5UNl2bpsszT4OAfN8Ufg
RB8GklU+UlvqtfEcm7Xh0iHvGe1PMm2k0DnVV4LMZNpZLs2i7syszZHIuf2EfdKe
wYyXwPE+SAejRoLrUz78zLYzzMv00YqKj0RDXV/4MWXa6543NAHtkKZjcCBGOvvX
tzvlq4I5CgjjY77Elh9v25RPfkVh6+m2qlRGgDHzz7NdzUfEwrw1oQhKlRqc/8ry
gw4Nu6Jx+Ndyr0QwH+yQKffHuGhCt6wjKlDe+SMCQQKCAQBKzTWjgsXY21X5MuXw
ZnLC8OHJwnEaHfq7MuWm/ClVGUnCGSTEq731kKTZFFpAteA1ShO8P180+sHc8fIq
DvGW0K+rklLy4zDSk/+HZSQIhFYse4FtAUOaUP4rYt5NQcAcINxc3aj1jxhbKhS6
njyQFQAinKjtKKVziz47PCz8ANvU7Gu3rhs4MAtzcbOO+9FltakjffWr25yknO4H
Aceuqv1VQo7rGjuTa5ZFjOqJ/Ua1ekxgrN8urAWRNIwiPBrWiZaxDunD2j7mq3kD
wl4DD6Ognbh7taGGnP1i05b6PSVNbQow2HYx24VWGAEA9Mx2Lqv3Yw9dwBiGTXgv
ild1AoIBAFmxC0YmFiKYDAPqpKwvs3P2FGYtZytQ7UZvxmgoLS7Hl2/6WepgUNC3
ta6SD8ur7zRdkdhplGjP55Oq6IKJxZIDFXreXQP5Hqgpm63SoP2u+QQgdKPBDO5+
RDGjLhEFxJrmzad8mmpk8a6eTSHZsV2lACaqeDQtDjMiOcBLGOL/p1EsuEp1XeZL
mHwC7tQVs4JqZgTsEFA+M9uq13XbfrVxhVdN2YjdwJy/KlcUic/YnDPrel99TFoN
BQaSxeV0YuCtV0IoaKfy6AYSLXw3SVwgF3vxIGvdtnmRWBW4Lo+hgIjnWxnL7fCJ
Zjue8EbO604aIx+Vbpz/PyfDxEbzdQECggEAc+RdYEuStS1jjhdBGIY/LvY94Oys
EeX57DhvkkiOJ7VWMDijIJxoirUIY7wuyjchhFK7eXL2OyXkA9mBCd667DUbBQ8C
VPI0av482Ups94N0gZl5jSwzu3KfWENzXNPEJLNMeDfJnAc1FYjLg+hSi/ZmdpMY
zqR4zfM4KFm62IedDlwlJTUenNK3UmxBDid9y2xar7Urzt1UC+scag/XcjJz5Y9v
CVZdjkrhjnMsC+6hb1YdldcadgZhea4R1oHF5vfqlALoLYMENEBXoE7LrulcQ6ue
72BZdoxOEzDjuDJJZ5DDml49pqhICYJoQXxZm0B8tHpdWZcR8uhm7FKrwg==
-----END RSA PRIVATE KEY-----"""

OAUTH2_PROVIDER = {
    "OIDC_ISS_ENDPOINT": "https://gallien.kde.org",
    "OIDC_USERINFO_ENDPOINT": "https://gallien.kde.org/userinfo/",
    "OIDC_RSA_PRIVATE_KEY": OIDC_RSA_PRIVATE_KEY,
}


# Update this to something unique for your machine.
# This was generated using "pwgen -sync 64"
SECRET_KEY = env('SECRET_KEY')

# For testing purposes, allow HTTP as well as HTTPS. Never enable this in production!
# OAUTH2_PROVIDER['ALLOWED_REDIRECT_URI_SCHEMES'] = ['http', 'https']

DATABASES = {
    'default': dj_database_url.parse(env('DATABASE_URL'), conn_max_age=600),
}


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'default': {
            'format': '%(asctime)-15s %(levelname)8s %(name)s %(message)s'
        },
        'verbose': {
            'format': '%(asctime)-15s %(levelname)8s %(name)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'default',  # Set to 'verbose' in production
            'stream': 'ext://sys.stderr',
        },
        # Enable this in production:
        # 'sentry': {
        #     'level': 'ERROR',  # To capture more than ERROR, change to WARNING, INFO, etc.
        #     'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        #     # 'tags': {'custom-tag': 'x'},
        # },
    },
    'loggers': {
        'bid_main': {'level': 'DEBUG'},
        'mykdeorg': {'level': 'DEBUG'},
        'bid_api': {'level': 'DEBUG'},
        'bid_addon_support': {'level': 'DEBUG'},
        'sentry.errors': {'level': 'DEBUG', 'handlers': ['console'], 'propagate': False},
    },
    'root': {
        'level': 'WARNING',
        'handlers': [
            'console',
            # Enable this in production:
            # 'sentry',
        ],
    }
}

# For Debug Toolbar, extend with whatever address you use to connect
# to your dev server.
INTERNAL_IPS = ['127.0.0.1']

ALLOWED_HOSTS = [env('ALLOWED_HOST')]

# Don't use this in production, but only in tests.
# ALLOWED_HOSTS = ['*']


# # Raven is the Sentry.io integration app for Django. Enable this on production:
# import os
# import raven
# INSTALLED_APPS.append('raven.contrib.django.raven_compat')
#
# RAVEN_CONFIG = {
#     'dsn': 'https://<key>:<secret>@sentry.io/<project>',
#     # If you are using git, you can also automatically configure the
#     # release based on the git info.
#     'release': raven.fetch_git_sha(os.path.abspath(os.curdir)),
# }

# For development, dump email to the console instead of trying to actually send it.
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# Alternatively, run python3 -m smtpd -n -c DebuggingServer -d '0.0.0.0:2525' and set
# EMAIL_PORT = 2525

EMAIL_HOST = 'localhost'
EMAIL_HOST_USER = "noreply@kde.org"

# Hosts that we allow redirecting to with a next=xxx parameter on the /login and /switch
# endpoints; this is an addition to the defaults, for development purposes.
# NEXT_REDIR_AFTER_LOGIN_ALLOWED_HOSTS.update({
#     'cloud.local:5000', 'cloud.local:5001', 'cloud.local',
# })
